﻿/*
Printer++ Virtual Printer Processor
Copyright (C) 2012 - Printer++

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
namespace SimplyLetter
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnProcess = new System.Windows.Forms.Button();
            this.fDialog = new System.Windows.Forms.OpenFileDialog();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.mnuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuTray_ShowHide = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTray_About = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTray_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupSimplexDuplex = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupPaperFormat = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.groupColor = new System.Windows.Forms.GroupBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mnuTray.SuspendLayout();
            this.groupSimplexDuplex.SuspendLayout();
            this.groupPaperFormat.SuspendLayout();
            this.groupColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(12, 362);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(405, 25);
            this.btnProcess.TabIndex = 0;
            this.btnProcess.Text = "Dokument als Brief versenden ";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenuStrip = this.mnuTray;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "SimplyLetter";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // mnuTray
            // 
            this.mnuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTray_ShowHide,
            this.mnuTray_About,
            this.mnuTray_Exit});
            this.mnuTray.Name = "contextMenuStrip1";
            this.mnuTray.Size = new System.Drawing.Size(212, 70);
            // 
            // mnuTray_ShowHide
            // 
            this.mnuTray_ShowHide.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuTray_ShowHide.Name = "mnuTray_ShowHide";
            this.mnuTray_ShowHide.Size = new System.Drawing.Size(211, 22);
            this.mnuTray_ShowHide.Text = "Show/Hide SimplyLetter";
            this.mnuTray_ShowHide.Click += new System.EventHandler(this.mnuTray_ShowHide_Click);
            // 
            // mnuTray_About
            // 
            this.mnuTray_About.Name = "mnuTray_About";
            this.mnuTray_About.Size = new System.Drawing.Size(211, 22);
            this.mnuTray_About.Text = "About";
            this.mnuTray_About.Visible = false;
            this.mnuTray_About.Click += new System.EventHandler(this.mnuTray_About_Click);
            // 
            // mnuTray_Exit
            // 
            this.mnuTray_Exit.Name = "mnuTray_Exit";
            this.mnuTray_Exit.Size = new System.Drawing.Size(211, 22);
            this.mnuTray_Exit.Text = "Shutdown SimplyLetter";
            this.mnuTray_Exit.Click += new System.EventHandler(this.mnuTray_Exit_Click);
            // 
            // groupSimplexDuplex
            // 
            this.groupSimplexDuplex.Controls.Add(this.radioButton2);
            this.groupSimplexDuplex.Controls.Add(this.radioButton1);
            this.groupSimplexDuplex.Location = new System.Drawing.Point(12, 138);
            this.groupSimplexDuplex.Name = "groupSimplexDuplex";
            this.groupSimplexDuplex.Size = new System.Drawing.Size(407, 52);
            this.groupSimplexDuplex.TabIndex = 1;
            this.groupSimplexDuplex.TabStop = false;
            this.groupSimplexDuplex.Text = "Simplex / Duplex";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(238, 22);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(68, 18);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Duplex";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(15, 22);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(73, 18);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Simplex";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // groupPaperFormat
            // 
            this.groupPaperFormat.Controls.Add(this.radioButton4);
            this.groupPaperFormat.Controls.Add(this.radioButton3);
            this.groupPaperFormat.Location = new System.Drawing.Point(12, 289);
            this.groupPaperFormat.Name = "groupPaperFormat";
            this.groupPaperFormat.Size = new System.Drawing.Size(405, 55);
            this.groupPaperFormat.TabIndex = 2;
            this.groupPaperFormat.TabStop = false;
            this.groupPaperFormat.Text = "Einwurfeinschreiben (+2,80€)";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(238, 21);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(38, 18);
            this.radioButton4.TabIndex = 1;
            this.radioButton4.Text = "Ja";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(15, 22);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(53, 18);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Nein";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // groupColor
            // 
            this.groupColor.Controls.Add(this.radioButton6);
            this.groupColor.Controls.Add(this.radioButton5);
            this.groupColor.Location = new System.Drawing.Point(12, 210);
            this.groupColor.Name = "groupColor";
            this.groupColor.Size = new System.Drawing.Size(405, 61);
            this.groupColor.TabIndex = 3;
            this.groupColor.TabStop = false;
            this.groupColor.Text = "Farbeinstellungen";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(238, 22);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(61, 18);
            this.radioButton6.TabIndex = 1;
            this.radioButton6.Text = "Farbe";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(15, 22);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(51, 18);
            this.radioButton5.TabIndex = 0;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "S/W";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "Druckeinstellungen";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SimplyLetter.Properties.Resources.SL_Logo_R;
            this.pictureBox1.Location = new System.Drawing.Point(12, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(407, 84);
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 399);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupColor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupPaperFormat);
            this.Controls.Add(this.groupSimplexDuplex);
            this.Controls.Add(this.btnProcess);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simply-Letter GmbH";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.mnuTray.ResumeLayout(false);
            this.groupSimplexDuplex.ResumeLayout(false);
            this.groupSimplexDuplex.PerformLayout();
            this.groupPaperFormat.ResumeLayout(false);
            this.groupPaperFormat.PerformLayout();
            this.groupColor.ResumeLayout(false);
            this.groupColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.OpenFileDialog fDialog;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip mnuTray;
        private System.Windows.Forms.ToolStripMenuItem mnuTray_Exit;
        private System.Windows.Forms.ToolStripMenuItem mnuTray_About;
        private System.Windows.Forms.ToolStripMenuItem mnuTray_ShowHide;
        private System.Windows.Forms.GroupBox groupSimplexDuplex;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupPaperFormat;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.GroupBox groupColor;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

