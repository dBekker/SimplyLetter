﻿/*
Printer++ Virtual Printer Processor
Copyright (C) 2012 - Printer++

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Windows.Forms;
using SimplyLetterSDK;
using System.IO;

namespace SimplyLetter
{

    public partial class frmMain : Form
    {
        private bool closedFromMenu = false;
        IOMonitorHelper iomh = null;

        public frmMain()
        {
            InitializeComponent();
        }

        public frmMain(bool silentMode, bool noMonitor, string key, string fileName)
        {
            try
            {
                System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
                IO.Log(string.Format("Starting SimplyLetter: ({0},{1},{2},{3})", silentMode, noMonitor, key, fileName));
                InitializeComponent();
                iomh = new IOMonitorHelper();
                /*
                if (silentMode == true)
                {
                    WindowState = FormWindowState.Minimized;
                    ShowInTaskbar = false;
                }
                */
                if (!noMonitor)
                    iomh.StartMonitor("C:\\SimplyLetter\\Temp");
                /*
                if (!string.IsNullOrWhiteSpace(key))
                    txtKey.Text = key;
                else
                    txtKey.Text = ConfigurationManager.AppSettings["DefaultProcessor"];


                if (!string.IsNullOrWhiteSpace(fileName))
                    txtFileName.Text = fileName;

                Process(key, fileName);
                */
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (!LicenseHelper.Validate())
            {
                MessageBox.Show("Bitte Registrieren Sie sich", "Simply-Letter GmbH");
                var regFrm = new frmRegistration();
                regFrm.ShowDialog();
            }
            btnProcess.Enabled = false;         
            var label = btnProcess.Text;
            btnProcess.Text = "Versand...";
            ProcessorHelper.Process();
            btnProcess.Text = label;
            MessageBox.Show("Ihre Datei wurde gesendet.", "Simply-Letter GmbH");
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = true;
            this.ShowInTaskbar = false;
            btnProcess.Enabled = true;
        }

        #region Form
        private void ShowError(Exception ex)
        {
            //IO.Log(ex.Message);
            MessageBox.Show(ex.Message, "Unhandled Exception");
        }
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
                closedFromMenu = true;

            if (closedFromMenu == false)
            {
                WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
                e.Cancel = true;
            }
            else
            {
                if (iomh != null)
                    iomh.StopMonitor();
            }
        }
        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mnuTray_ShowHide_Click(sender, e);
        }
        #endregion

        #region mnuTray
        private void mnuTray_Exit_Click(object sender, EventArgs e)
        {
            closedFromMenu = true;
            this.Close();
        }
        private void mnuTray_About_Click(object sender, EventArgs e)
        {

        }
        private void mnuTray_ShowHide_Click(object sender, EventArgs e)
        {
            //this.Visible = !this.Visible;
            //if (!this.ShowInTaskbar)
            //    this.ShowInTaskbar = true;
            this.ShowInTaskbar = !this.ShowInTaskbar;

            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;
            else
                this.WindowState = FormWindowState.Minimized;
        }
        #endregion

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocSimplexDouplex = 0;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocSimplexDouplex = 1;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocColor = 0;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocColor = 1;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocShipment = 0;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            ProcessorHelper.DocShipment = 1;
        }
    }
}
