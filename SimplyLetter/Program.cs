﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using System.Windows.Forms;

namespace SimplyLetter
{
    static class Program
    {
        public static Form frm;
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var silentMode = false;
            var noMonitor = false;
            var key = string.Empty;
            var fileName = string.Empty;

            //-silent -nomonitor -key Elfa -file C:\SimplyLetter\Temp\Elfa_PH-D-W7-1_rsaturnino_20120622_133136_22.ps
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i].ToLower())
                {
                    
                    case "-silent":
                        silentMode = true;
                        break;               
                    case "-nomonitor":
                        noMonitor = true;
                        break;
                    case "-key":
                        i += 1;
                        key = args[i];
                        break;
                    case "-file":
                        i += 1;
                        fileName = args[i];
                        break;

                    default:
                        break;
                }
            }

            frm = new frmMain(silentMode, noMonitor, key, fileName);
            Application.Run(frm);
        }
    }
}
