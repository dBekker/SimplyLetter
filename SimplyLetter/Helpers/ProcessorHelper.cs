﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using SimplyLetterSDK;
using System.IO;
using Microsoft.Win32;

namespace SimplyLetter
{
    public class ProcessorHelper
    {

        public static string customerID = "null";

        public static double DocSimplexDouplex = 0;

        public static double DocPageNumbers = 0;

        public static double DocMassMail = 0;

        public static double DocShipment = 0;

        public static double DocColor = 0;

        public static void ShowRegForm()
        {
            var frm = new frmRegistration();
            frm.ShowDialog();            
        }

        public static string psFilename;
        public static string txtFilename;

        public static void Process()
        {
            try
            {

                IO.Log(string.Format("Processing: {0}", psFilename));

                var txtFilename = System.IO.Path.GetTempFileName();
                txtFilename = System.IO.Path.ChangeExtension(txtFilename, "txt");
                using (StreamWriter sw = File.CreateText(txtFilename))
                {
                    sw.WriteLine("=== Start File Settings ===");
                    sw.WriteLine("Customer-ID: " + customerID);
                    sw.WriteLine("Page numbers: " + DocPageNumbers);
                    sw.WriteLine("Page print: " + DocSimplexDouplex);
                    sw.WriteLine("Massmail: " + DocMassMail);
                    sw.WriteLine("Shimpent: " + DocShipment);
                    sw.WriteLine("Color: " + DocColor);
                    sw.WriteLine("=== End File Settings ===");
                    string keyName = string.Format(@"Software\SimplyLetter");
                    Registry.CurrentUser.CreateSubKey(keyName);
                    RegistryKey regKey = Registry.CurrentUser.OpenSubKey(keyName);
                    sw.WriteLine(" ");
                    sw.WriteLine("=== Start User Settings ===");
                    sw.WriteLine("Firmenname: " + regKey.GetValue("Firmenname"));
                    sw.WriteLine("Name: " + regKey.GetValue("Name"));
                    sw.WriteLine("Vorname: " + regKey.GetValue("Vorname"));
                    sw.WriteLine("Tel: " + regKey.GetValue("Tel"));
                    sw.WriteLine("Email: " + regKey.GetValue("Email"));
                    sw.WriteLine("=== End User Settings ===");
                    regKey.Close();
                }

                var pdfFilename = System.IO.Path.GetTempFileName();
                pdfFilename = System.IO.Path.ChangeExtension(pdfFilename, "pdf");
                SimplyLetterSDK.Converters.Ghostscript.PSToPdf(psFilename, pdfFilename);
                File.Delete(psFilename);
                EmailHelper.DocEmail(txtFilename, pdfFilename);

            }
            catch (Exception ex)
            {
                IO.Log(ex.Message);
            }
        }
    }
}
