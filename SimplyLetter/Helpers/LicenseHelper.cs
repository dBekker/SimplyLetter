﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using Microsoft.Win32;

namespace SimplyLetter
{
    public static class LicenseHelper
    {
        public static string GetKey()
        {
            //return CryptographyHelper.Encrypt(GetMachineGuid());
            return GetKey(GetMachineGuid());
        }

        public static string GetKey(string machineId)
        {
            return CryptographyHelper.Encrypt(machineId);
        }

        public static string CleanRegKey(string value)
        {
            return value;
        }

        public static bool Register(string regKeyInput)
        {
            var retVal = false;
            var regKey = GetKey();
            if (regKey.Equals(regKeyInput, StringComparison.OrdinalIgnoreCase))
            {
                ConfigHelper.AddSetting("RegKey", regKey);
                retVal = true;
            }
            return retVal;
        }

        public static bool Validate()
        {
            try
            {
                string keyName = string.Format(@"Software\SimplyLetter");
                Registry.CurrentUser.CreateSubKey(keyName);
                RegistryKey regKey = Registry.CurrentUser.OpenSubKey(keyName);
                var reg = regKey.GetValue("Registered");
                regKey.Close();
                if((Int32)reg == 1)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                return false;
            }

            //var regKey = ConfigHelper.ReadSetting("RegKey");
            //var regKeyCalculated = CryptographyHelper.Encrypt(GetMachineGuid());
            //var retVal = false;
            //if (!string.IsNullOrWhiteSpace(regKey))
            //    retVal = (regKey.Equals(regKeyCalculated, StringComparison.OrdinalIgnoreCase));
            //return retVal;
            return true;
        }

        public static string GetMachineGuid()
        {
            var retVal = string.Empty;
            string regPath = @"SOFTWARE\Microsoft\Cryptography";
            string regKey = "MachineGuid";
            string value32 = RegistryHelper.ReadRegistry32(regPath, regKey);
            string value64 = RegistryHelper.ReadRegistry64(regPath, regKey);

            if (!string.IsNullOrWhiteSpace(value32))
                retVal = value32;
            else if (!string.IsNullOrWhiteSpace(value64))
                retVal = value64;

            return retVal;
        }
    }
}
