﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using Microsoft.Win32;
namespace SimplyLetter
{
    public static class RegistryHelper
    {
        public static string ReadRegistry32(string path, string key)
        {
            return ReadRegistry(path, key, RegistryView.Registry32);
        }

        public static string ReadRegistry64(string path, string key)
        {
            return ReadRegistry(path, key, RegistryView.Registry64);
        }

        public static string ReadRegistry(string path, string key, RegistryView view)
        {
            var retVal = string.Empty;
            RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, view);
            localKey = localKey.OpenSubKey(path);
            if (localKey != null)
            {
                retVal = (string)localKey.GetValue(key);
            }
            return retVal;
        }
    }
}
