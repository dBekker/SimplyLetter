﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using System.IO;
using System.Security.Permissions;
using SimplyLetterSDK;
using System.Windows.Forms;

namespace SimplyLetter
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class IOMonitorHelper
    {
        private FileSystemWatcher fsw = null;

        public void StartMonitor(string path)
        {
            if (System.IO.Directory.Exists(path))
            {
                fsw = new FileSystemWatcher(path, "*.ps");
                fsw.NotifyFilter = System.IO.NotifyFilters.DirectoryName;

                fsw.NotifyFilter = fsw.NotifyFilter | System.IO.NotifyFilters.FileName;
                fsw.NotifyFilter = fsw.NotifyFilter | System.IO.NotifyFilters.Attributes;

                fsw.Created += new FileSystemEventHandler(fsw_eventHandler);
                //fsw.Changed += new FileSystemEventHandler(fsw_eventHandler);
                //fsw.Deleted += new FileSystemEventHandler(fsw_eventHandler);
                //fsw.Renamed += new RenamedEventHandler(fsw_eventHandler);

                try
                {
                    fsw.EnableRaisingEvents = true;
                }
                catch (ArgumentException ex)
                {
                    IO.Log(ex.Message);
                    throw;
                }
            }
            else
            {
                IO.Log(string.Format("Unable to monitor folder: {0}. Folder does not exist.", path));
            }
        }

        public void StopMonitor()
        {
            if (fsw != null)
            {
                fsw.EnableRaisingEvents = false;
                fsw.Dispose();
            }
        }

        private void fsw_eventHandler(object sender, System.IO.FileSystemEventArgs e)
        {
            var sleepTimeout = 100;
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    while (IO.IsFileLocked(e.FullPath) == true)
                    {
                        IO.Log("Locked...Sleeping...");
                        System.Threading.Thread.Sleep(sleepTimeout);
                    }
                    IO.Log(string.Format("Created: {0}", e.FullPath));
                    ProcessorHelper.psFilename = e.FullPath;
                    Program.frm.WindowState = FormWindowState.Normal;
                    //Program.frm.ShowInTaskbar = true;
                    break;
                //case WatcherChangeTypes.Changed:
                //    IO.Log(string.Format("Changed: {0}", e.FullPath));
                //    break;
                //case WatcherChangeTypes.Deleted:
                //    IO.Log(string.Format("Deleted: {0}", e.FullPath));
                //    break;
                //case WatcherChangeTypes.Renamed:
                //    IO.Log(string.Format("Renamed: {0}", e.FullPath));
                //    break;
                default: // Another action
                    break;
            }
        }

        
    }
}
