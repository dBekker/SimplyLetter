﻿/*
SimplyLetter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace SimplyLetter
{
    public class EmailHelper
    {
        private const string SMTP_HOST = "smtp.mailgun.org";
        private const string  SENDER = "postmaster@mg.simply-letter.com";
        private const string LOGIN = "postmaster@mg.simply-letter.com";
        private const string PASSWORD = "755d8d563e6624924612e69029b7868d-8b7bf2f1-b7142d0b";

        public static void DocEmail(string txtFilename, string pdfFilename)
        {

            var recepient = "support@simply-letter.com";
            var fileNamePDF = System.IO.Path.GetFileName(pdfFilename);
            var fileNameTXT = System.IO.Path.GetFileName(System.IO.Path.ChangeExtension(pdfFilename, "txt"));
            SmtpClient client = new SmtpClient(SMTP_HOST, 587);
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(LOGIN, PASSWORD);

            MailAddress from = new MailAddress(SENDER);
            MailAddress to = new MailAddress(recepient);
            MailMessage message = new MailMessage(from, to);
            message.Body = "\r\n Hallo! \r\n\r\n Bitte führen Sie nachstehenden " + fileNamePDF + ". \r\n\r\n Auftrag aus! \r\n\r\n Simply-Letter GmbH";
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = "Simply-Letter - Neuauftrag: " + fileNamePDF;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
            contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
            contentType.Name = fileNamePDF;
            message.Attachments.Add(new Attachment(pdfFilename, contentType));
            System.Net.Mime.ContentType contentTxtType = new System.Net.Mime.ContentType();
            contentTxtType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
            contentTxtType.Name = fileNameTXT;
            message.Attachments.Add(new Attachment(txtFilename, contentTxtType));
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.Send(message);
            message.Dispose();
        }

        public static void RegEmail(string txtFirmenname, string txtName, string txtVorname, string txtStrasse, string txtPLZ, string txtOrt, string txtTel, string txtFax, string txtEmail)
        {
            var recepient = "service@simply-letter.com";
            SmtpClient client = new SmtpClient(SMTP_HOST, 587);
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(LOGIN, PASSWORD);

            MailAddress from = new MailAddress(SENDER);
            MailAddress to = new MailAddress(recepient);
            MailMessage message = new MailMessage(from, to);
            message.Body = "\r\n Hallo! Ein Neukunde hat sich angemeldet \r\n\r\n " +
                "Firmenname: " + txtFirmenname + " \r\n\r\n " +
                "Name: " + txtName + " \r\n\r\n " +
                "Vorname: " + txtVorname + " \r\n\r\n " +
                "Strasse: " + txtStrasse + " \r\n\r\n " +
                "PLZ: " + txtPLZ + " \r\n\r\n " +
                "Ort: " + txtOrt + " \r\n\r\n " +
                "Tel: " + txtTel + " \r\n\r\n " +
                "Fax: " + txtFax + " \r\n\r\n " +
                "Email: " + txtEmail + " \r\n\r\n " +
                "Auftrag aus! \r\n\r\n Simply-Letter GmbH";
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = "Simply Letter Neukunde: " + txtFirmenname;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.Send(message);
            message.Dispose();
        }
    }
}
