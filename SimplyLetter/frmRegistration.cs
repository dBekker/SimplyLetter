﻿/*
Printer++ Virtual Printer Processor
Copyright (C) 2012 - Printer++

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace SimplyLetter
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {

            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(txtFirmenname.Text) &&
                !string.IsNullOrWhiteSpace(txtName.Text) &&
                !string.IsNullOrWhiteSpace(txtVorname.Text) &&
                !string.IsNullOrWhiteSpace(txtStrasse.Text) &&
                !string.IsNullOrWhiteSpace(txtPLZ.Text) &&
                !string.IsNullOrWhiteSpace(txtOrt.Text) &&
                !string.IsNullOrWhiteSpace(txtTel.Text) &&
                !string.IsNullOrWhiteSpace(txtFax.Text) &&
                !string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Vielen Dank für Ihre Registrierung Ihr Simply-Letter Team ", "Simply-Letter GmbH");
                try
                {

                    EmailHelper.RegEmail(txtFirmenname.Text, txtName.Text, txtVorname.Text, txtStrasse.Text, txtPLZ.Text, txtOrt.Text, txtTel.Text, txtFax.Text, txtEmail.Text);

                    string keyName = string.Format(@"Software\SimplyLetter");
                    Registry.CurrentUser.CreateSubKey(keyName);
                    RegistryKey regKey = Registry.CurrentUser.OpenSubKey(keyName, true);
                    regKey.SetValue("Registered", 1, RegistryValueKind.DWord);
                    regKey.SetValue("Firmenname", txtFirmenname.Text, RegistryValueKind.String);
                    regKey.SetValue("Name", txtName.Text, RegistryValueKind.String);
                    regKey.SetValue("Vorname", txtVorname.Text, RegistryValueKind.String);
                    regKey.SetValue("Strasse", txtStrasse.Text, RegistryValueKind.String);
                    regKey.SetValue("PLZ", txtPLZ.Text, RegistryValueKind.String);
                    regKey.SetValue("Ort", txtOrt.Text, RegistryValueKind.String);
                    regKey.SetValue("Tel", txtTel.Text, RegistryValueKind.String);
                    regKey.SetValue("Fax", txtFax.Text, RegistryValueKind.String);
                    regKey.SetValue("Email", txtEmail.Text, RegistryValueKind.String);
                    regKey.Close();
                }
                catch (Exception ex)
                {
                    //Error occured while sending email. Add code to handle error.
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Bitte füllen Sie nachstehende Felder aus!", "Simply-Letter GmbH");
            }
        }

        private void frmRegistration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }
    }
}
