﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

namespace SimplyLetterSDK
{
    [System.Diagnostics.DebuggerStepThrough]
    public class Processor
    {
        IProcessor processorObject;

        public Processor(IProcessor processorClass)
        {
            this.processorObject = processorClass;
        }

        public ProcessResult Process(string key, string postscriptInput)
        {
            return processorObject.Process(key, postscriptInput);
        }
    }
}
