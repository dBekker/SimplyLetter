﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

namespace SimplyLetterSDK.Converters
{
    public static class PdfToText
    {
        public static string Convert(string pdfFilename, string txtFilename)
        {
            var retVal = string.Empty;
            var errorMessage = string.Empty;

            var command = "C:\\SimplyLetter\\Converters\\pdftotext\\pdftotext.exe";
            var args = string.Format("-q -f 1 -l 100 \"{0}\" \"{1}\"", pdfFilename, txtFilename);
            retVal = Shell.ExecuteShellCommand(command, args, ref errorMessage);
            return retVal;
        }
    }
}
