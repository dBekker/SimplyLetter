﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

namespace SimplyLetterSDK.Converters
{
    public static class Ghostscript
    {
        public static string PSToTxt(string psFilename)
        {
            var retVal = string.Empty;
            var errorMessage = string.Empty;

            var command = "C:\\SimplyLetter\\Converters\\gs\\gswin32c";
            var args = string.Format("-q -dNODISPLAY -P- -dSAFER -dDELAYBIND -dWRITESYSTEMDICT -dSIMPLE \"c:\\SimplyLetter\\Converters\\gs\\ps2ascii.ps\" \"{0}\" -c quit", psFilename);
            retVal = Shell.ExecuteShellCommand(command, args, ref errorMessage);
            return retVal;
        }

        public static string PSToPdf(string psFilename, string pdfFilename)
        {
            var retVal = string.Empty;
            var errorMessage = string.Empty;

            var command = "C:\\SimplyLetter\\Converters\\gs\\gswin32c";
            var args = string.Format("-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=\"{1}\" -c save pop -f \"{0}\"", psFilename, pdfFilename);
            retVal = Shell.ExecuteShellCommand(command, args, ref errorMessage);
            return retVal;
        }

    }
}
