﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
namespace SimplyLetterSDK
{
    public class ProcessResult
    {
        public ProcessResult()
        {
            OutputFileName = null;
            HasException = false;
            Exception = null;
        }
        public ProcessResult(string outputFileName, bool hasException, Exception exception)
        {
            OutputFileName = outputFileName;
            HasException = hasException;
            Exception = exception;
        }
        public ProcessResult(Exception exception)
        {
            OutputFileName = null;
            HasException = true;
            Exception = exception;
        }
        public string OutputFileName { get; set; }
        public bool HasException { get; set; }
        public Exception Exception { get; set; }
    }
}
