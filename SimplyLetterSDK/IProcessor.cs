﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

namespace SimplyLetterSDK
{
    public interface IProcessor
    {
        ProcessResult Process(string key, string psFilename);
    }
}
