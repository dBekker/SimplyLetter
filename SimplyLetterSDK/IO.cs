﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
using System.Configuration;
using System.IO;

namespace SimplyLetterSDK
{
    public static class IO
    {
        public static string ReadFile(string filename)
        {
            var retVal = string.Empty;
            using (var sr = new StreamReader(filename))
            {
                retVal = sr.ReadToEnd();
            }
            return retVal;
        }

        public static void WriteFile(string filename, string value)
        {
            using (var sw = new StreamWriter(filename, false))
            {
                sw.Write(value);
                sw.Flush();
            }
        }

        public static void Delete(string filename)
        {
            File.Delete(filename);
        }

        //public static void Print(string filename)
        //{
        //    StartProcess(filename, "print");
        //}

        //public static void StartProcess(string filename)
        //{
        //    StartProcess(filename, string.Empty);
        //}

        //public static void StartProcess(string filename, string startInfoVerb)
        //{
        //    Process p = new Process();
        //    p.StartInfo.FileName = filename;
        //    p.StartInfo.UseShellExecute = true;
        //    if (!string.IsNullOrWhiteSpace(startInfoVerb))
        //        p.StartInfo.Verb = startInfoVerb;
        //    p.EnableRaisingEvents = true;
        //    p.Exited += new System.EventHandler(printJob_Exited);
        //    p.Start();
            
        //}

        //static void printJob_Exited(object sender, System.EventArgs e)
        //{
        //    eventHandled = true;
        //}

        public static void Log(string msg)
        {
            var debugMode = ConfigurationManager.AppSettings["DebugMode"].ToBool();
            if (debugMode == true)
            {
                var appDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var fileName = string.Format("{0}\\SimplyLetter.log", appDir);
                using (var sw = new System.IO.StreamWriter(fileName, true))
                {
                    sw.Write(string.Format("{0} - {1}\n", DateTime.Now, msg));
                    sw.Flush();
                }
            }
        }

        public static bool IsFileLocked(string filename)
        {
            FileStream stream = null;
            FileInfo file = new FileInfo(filename);
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
    }
}
