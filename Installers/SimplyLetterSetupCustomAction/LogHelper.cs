﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using System;
namespace SimplyLetterSetupCustomAction
{
    public static class LogHelper
    {
        public static void Log(string msg)
        {
            var filename = "C:\\SimplyLetter_Installer.txt";
            using (var sw = new System.IO.StreamWriter(filename, true))
            {
                sw.Write(string.Format("{0} - {1}\n", DateTime.Now, msg));
                sw.Flush();
            }
        }
    }
}
