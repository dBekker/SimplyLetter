﻿/*
Simply-Letter Virtual Printer 
Copyright (C) 2018 - Simply-Letter
*/

using Microsoft.Win32;

namespace SimplyLetterSetupCustomAction
{
    public static class AutorunHelper
    {
        public static void AddToStartup()
        {
            LogHelper.Log("Adding SimplyLetter to Startup");
            var regKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            regKey.SetValue("SimplyLetter", (string)"C:\\SimplyLetter\\SimplyLetter.exe -silent", RegistryValueKind.String);
        }
    }
}
